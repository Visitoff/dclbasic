﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class ChoiceMechanic : MonoBehaviour
{

    [SerializeField]
    GameObject rayPoint;
    [SerializeField]
    GameObject abalakChoiceScreen;
    public SteamVR_Action_Boolean f_GrabPinch = null;
    [SerializeField]
    FlyMechanics flyMechanics;
    [SerializeField]
    GameObject player;
    [SerializeField]
    GameObject cylinder;

    void Start()
    {
        cylinder.SetActive(true);
        abalakChoiceScreen.SetActive(false);
        DontDestroyOnLoad(player);
        flyMechanics.enabled = false;
    }

    void Update()
    {
        Vector3 fwd = rayPoint.transform.TransformDirection(Vector3.forward);
        RaycastHit hit;
        if (Physics.Raycast(transform.position, fwd, out hit, 1000))
        {
           
            if (f_GrabPinch.state&& hit.transform.tag == "abalakChoice")
            {
                Debug.Log("abobus");
                abalakChoiceScreen.SetActive(true);
            }
            if (f_GrabPinch.state && hit.transform.tag == "AbalakStartButton")
            {
                Debug.Log("startButtonAbalak");
                SceneManager.LoadScene("Abalak_Main", LoadSceneMode.Single);
                cylinder.SetActive(false);
            }
         
        }
    }
}
