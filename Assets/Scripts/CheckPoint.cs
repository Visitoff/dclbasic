﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    public ScenarioAbalak scenario;

    private void OnTriggerEnter(Collider other)
    {

        scenario.checkPointCount++;
        this.gameObject.SetActive(false);
    }
}
