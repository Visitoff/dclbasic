﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScenarioAbalak : MonoBehaviour
{
    [SerializeField]
    GameObject spawnPoint;
    [SerializeField]
    List<CheckPoint> checkPoints;
    public List<ParticleSystem> partFinish;
    public List<ParticleSystem> partGorynych;
    public int checkPointCount = 0;
    float sessionTime;
    float timeToStartParticle = 0f;
    public Canvas canvas;
    public Text myText;
    public GameObject winScreen;
    GameObject player;
    FlyMechanics flyMechanics;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        flyMechanics = player.GetComponent<FlyMechanics>();
        flyMechanics.enabled = true;
        player.transform.position = spawnPoint.transform.position;
        player.transform.rotation = spawnPoint.transform.rotation;
        //checkPoints[0].gameObject.SetActive(false);
        canvas.worldCamera = player.gameObject.GetComponent<Camera>();
        winScreen.SetActive(false);
        StartCoroutine(WinScreenActivator());
        Debug.Log("asd");
        StartCoroutine(gorynichFire());
    }

    void Update()
    {
        if (checkPointCount >= 0 && checkPointCount < checkPoints.Count - 1)
        {
            checkPoints[checkPointCount + 1].gameObject.SetActive(true);
        }

        if (checkPoints.Count <= checkPointCount)
        {

        }
        if (checkPoints.Count <= checkPointCount + 1)
        {

            timeToStartParticle += Time.deltaTime;
            if (timeToStartParticle > 1f)
            {
                foreach (ParticleSystem part in partFinish)
                {
                    part.Play(true);
                }
                timeToStartParticle = 0f;
            }

        }
        sessionTime += Time.deltaTime;
    }
    IEnumerator WinScreenActivator()
    {
        yield return new WaitWhile(() => checkPoints.Count >= checkPointCount + 1);
        Debug.Log("aboba");
        player.transform.position = spawnPoint.transform.position;
        player.transform.rotation = spawnPoint.transform.rotation;
        flyMechanics.enabled = false;
        winScreen.SetActive(true);
        myText.text = string.Format("{0: 0}мин:{1:0}сек", sessionTime / 60, sessionTime % 60);
    }
    IEnumerator gorynichFire()
    {
        yield return new WaitWhile(() => checkPointCount != 3);
        Debug.Log("gorinych");
        foreach (ParticleSystem part in partGorynych)
        {
            part.Play(true);
        }

    }
}