﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class FlyMechanics : MonoBehaviour
{
    public float f_sensetivity = 8f;
    public float f_MaxSpeed = 10f;

    public SteamVR_Action_Boolean f_Movepress = null;
    public SteamVR_Action_Boolean f_GrabPinch = null;
    float smoothTime = 0.3f;
    float yVelocity = 0.0f;

    public SteamVR_Action_Vector2 f_MoveValue = null;

    //public SteamVR_Action_Vector2 f_RotatiomRight = null;
    //public SteamVR_Action_Vector2 f_RotatiomLeft = null;

    public float slowStopValue = 0.96f;
    float f_Speed = 0.0f;
    float f_SideMov = 0.0f;

    public float f_Jump = 3;
    public float f_Gravity = -5;
    Transform target;
    CharacterController f_CharControl = null;
    public Transform f_CameraRig = null;
    public Transform f_Head = null;

    private void Awake()
    {
        f_CharControl = GetComponent<CharacterController>();
    }
    void Start()
    {
        f_CameraRig = SteamVR_Render.Top().origin;
        f_Head = SteamVR_Render.Top().head;
    }

    void Update()
    {
        HandleHead();
        HandleHeight();
        CalculateMovement();
        PlayerRotation();
    }
    void HandleHead()
    {
        Vector3 oldPosition = f_CameraRig.position;
        Quaternion oldRotation = f_CameraRig.rotation;

        transform.eulerAngles = new Vector3(0.0f, f_Head.rotation.eulerAngles.y, 0.0f);

        f_CameraRig.position = oldPosition;
        f_CameraRig.rotation = oldRotation;
    }
    void CalculateMovement()
    {
        Vector3 orientationEuler = new Vector3(0, transform.eulerAngles.y, 0);
        Quaternion orientation = Quaternion.Euler(orientationEuler);
        Vector3 movement = Vector3.zero;
        if (f_Movepress.GetLastStateUp(SteamVR_Input_Sources.Any))
        {
            f_Speed *= 0;
            f_SideMov *= 0;
        }
        if (f_GrabPinch.state)
        {
            movement += (f_Jump * Vector3.up) * Time.deltaTime;
        }
        if (f_Movepress.state)
        {
            f_Speed = f_MoveValue.axis.y * f_sensetivity;
            f_SideMov = f_MoveValue.axis.x * f_sensetivity;

            movement += orientation * (f_Speed * Vector3.forward) * Time.deltaTime;
            movement += orientation * (f_SideMov * Vector3.right) * Time.deltaTime;
        }

        //if (f_Speed > 0.3f && f_SideMov > 0.3f)
        //{

        //    f_Speed *= slowStopValue;
        //    f_SideMov *= slowStopValue;

        //    movement += orientation * (f_Speed * Vector3.forward) * Time.deltaTime;
        //    movement += orientation * (f_SideMov * Vector3.right) * Time.deltaTime;
        //}
        //if(f_Speed < 0.3f && f_SideMov < 0.3f && !f_Movepress.state)
        //{
        //    f_Speed = 0;

        //    f_SideMov = 0;

        //    movement += orientation * (f_Speed * Vector3.forward) * Time.deltaTime;
        //    movement += orientation * (f_SideMov * Vector3.right) * Time.deltaTime;
        //}
        movement += (Vector3.up * f_Gravity) * Time.deltaTime;
        f_CharControl.Move(movement);
    }

    void HandleHeight()
    {
        float headlHeight = Mathf.Clamp(f_Head.localPosition.y, 1, 2);
        f_CharControl.height = headlHeight;

        Vector3 newCenter = Vector3.zero;
        newCenter.y = f_CharControl.height / 2;
        newCenter.y += f_CharControl.skinWidth;

        newCenter.x = f_Head.localPosition.x;
        newCenter.z = f_Head.localPosition.z;

        newCenter = Quaternion.Euler(0, -transform.eulerAngles.y, 0) * newCenter;

        f_CharControl.center = newCenter;
    }
    void PlayerRotation()
    {
        if (SteamVR_Input.GetStateDown("SnapTurnLeft", SteamVR_Input_Sources.RightHand))
        {
            SteamVR_Fade.Start(Color.black, 0);
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y - 45, transform.localEulerAngles.z);
            SteamVR_Fade.Start(Color.clear, 1);
        }
        if (SteamVR_Input.GetStateDown("SnapTurnRight", SteamVR_Input_Sources.RightHand))
        {
            SteamVR_Fade.Start(Color.black, 0);
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y + 45, transform.localEulerAngles.z);
            SteamVR_Fade.Start(Color.clear, 1);
        }

    }
}
