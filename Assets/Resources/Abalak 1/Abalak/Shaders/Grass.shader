﻿// Upgrade NOTE: upgraded instancing buffer 'Props' to new syntax.

Shader "Custom/Grass" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Cutoff ("Cutoff", Range(0,1)) = 0.5
	}
	SubShader {
		Tags { "RenderType"="TransparentCutout" "Queue"="Alphatest" }
		LOD 200
		
		Cull Off
		
		CGPROGRAM

		#pragma surface surf Lambert vertex:vert alphatest:_Cutoff noshadowmask nolppv nometa 

		#pragma target 3.0

		sampler2D _MainTex;

		struct Input
		{
			float2 uv_MainTex;
			float4 color : COLOR;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		UNITY_INSTANCING_BUFFER_START(Props)
			
		UNITY_INSTANCING_BUFFER_END(Props)
		
		inline fixed rand(float2 co)
		{
			return frac(sin(dot(co ,half2(12.9898, 78.233))) * 43758.5453);
		}
		
		void vert(inout appdata_full v)
		{
			
			float4 wPos = mul(unity_ObjectToWorld, v.vertex);
			
			float3 nN = normalize(v.normal);
			float3 wN = mul((float3x3)unity_ObjectToWorld, v.normal);
			
			#define SPEED 50
			#define BEND_FORCE 0.01
			
			wPos.xyz += sin((_Time.x+rand(v.vertex.xz))*SPEED) * wN * BEND_FORCE * v.color.r; // анимация
			
			v.vertex = mul(unity_WorldToObject,wPos);
			
			v.normal = mul((float3x3)unity_WorldToObject,float3(0,1,0)) * 0.9 + nN * 0.1; // нормали травы смотрят только вверх, исправление неверного затемнения задних граней при Cull Off
		}

		void surf (Input IN, inout SurfaceOutput o)
		{
			
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			
			c.rgb *= IN.color.r * 0.2 + 0.8; // затенение к низу
			
			o.Albedo = c.rgb;

			o.Alpha = c.a;
		}
		ENDCG
	}
	Fallback "Transparent/Cutout/VertexLit"
}
