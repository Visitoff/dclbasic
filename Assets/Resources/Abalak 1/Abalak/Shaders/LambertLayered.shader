﻿// Upgrade NOTE: upgraded instancing buffer 'Props' to new syntax.

Shader "Custom/LambertLayered"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_NormalMap ("Normals (RGB)", 2D) = "bump" {}
		_L1 ("L1", 2D) = "white" {}
		_L1N ("L1N", 2D) = "bump" {}
		_L2 ("L2", 2D) = "white" {}
		_L2N ("L2N", 2D) = "bump" {}
		_Mask ("Mask", 2D) = "black" {}
		_MaskPos ("Mask position", Vector) = (0,100,0)
		[Toggle(L1)] _usel1 ("Use layer 1", Float) = 1
		[Toggle(L2)] _usel2 ("Use layer 2", Float) = 1
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		
		#pragma surface surf Lambert fullforwardshadows

		#pragma target 3.0
		
		#pragma multi_compile L1 __
		#pragma multi_compile L2 __
		
		sampler2D _MainTex;
		sampler2D _NormalMap;
		sampler2D _L1;
		sampler2D _L1N;
		sampler2D _L2;
		sampler2D _L2N;
		sampler2D _Mask;

		struct Input
		{
			float2 uv_MainTex;
			float2 uv_L1;
			float2 uv2_Mask;
			float3 worldPos;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		float4 _MaskPos;
		
		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutput o)
		{
			
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			fixed3 n = UnpackNormal(tex2D (_NormalMap, IN.uv_MainTex));
			
			float2 maskUV = (IN.worldPos.xz - _MaskPos.xz)/_MaskPos.y + .5;
			maskUV.x=1.0-maskUV.x;
			fixed4 m = tex2D (_Mask, maskUV);
			
			float2 lUV = IN.worldPos.xz / 2;
			
			fixed4 l;
			fixed3 ln;
			fixed f;
			
			#ifdef L1
				l = tex2D (_L1, lUV);
				ln = UnpackNormal(tex2D (_L1N, lUV));
				f = saturate(saturate(m.r*2-1.0+l.a)*2);
				c.rgb = lerp(c.rgb, l.rgb, f);
				n = lerp(n, ln, f);
			#endif
			
			#ifdef L2
				l = tex2D (_L2, lUV);
				ln = UnpackNormal(tex2D (_L2N, lUV));
				f = saturate(saturate(m.g*2-1.0+l.a)*2);
				c.rgb = lerp(c.rgb, l.rgb, f);
				n = lerp(n, ln, f);
			#endif
			
			n.xy *= 2;
			
			o.Normal = n;
			o.Albedo = c.rgb;
			o.Specular=.2;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
