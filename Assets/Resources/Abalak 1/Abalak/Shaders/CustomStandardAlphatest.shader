﻿Shader "Custom/CustomStandardAlphatest"
{
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Normal ("Normal", 2D) = "bump" {}
		_GlossinessMap ("GlossinessMap", 2D) = "white" {}
		_GlowMap ("GlowMap", 2D) = "white" {}
		_Reflection ("ReflectionMap", CUBE) = "" {}
		_ReflectionForce ("Reflection force", Range(0,1)) = 1
		_ReflectionSmooth ("Reflection smooth", Range(0,10)) = 0
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_Radiosity ("Radiosity force", Range(0,1)) = 0.8
		_Bump ("Bump", Range(0,10)) = 1.0
		_Cutoff ("Cutoff", Range(0,1)) = 1.0
		//[Toggle] REFLECTIONS("Use reflections", Float) = 0
		//[Toggle] REFLECTIONSMASK("Use gloss map in reflections", Float) = 0
		//[Toggle] GLOWMAP("Use glowmap", Float) = 0
		[Toggle] VERTEXCOLOR("Vertex color to albedo", Float) = 0
		//[Toggle] RADIOSITI("Use radiositi", Float) = 0
		[Toggle] RADIOSITI_FROM_ALPHA("Use radiositi from alpha", Float) = 0
	}
	SubShader {
		Tags { "RenderType"="TransparentCutout" "Queue"="Alphatest" }
		LOD 200
		
		CGPROGRAM
		
		#pragma surface surf Standard fullforwardshadows alphatest:_Cutoff

		#pragma target 3.0
		
		//#pragma multi_compile REFLECTIONS_ON __
		//#pragma multi_compile REFLECTIONSMASK_ON __
		//#pragma multi_compile GLOWMAP_ON __
		#pragma multi_compile VERTEXCOLOR_ON __
		//#pragma multi_compile RADIOSITI_ON __
		#pragma multi_compile RADIOSITI_FROM_ALPHA_ON __

		#include "CGbase.cginc"
		
		ENDCG
	}
	Fallback "Transparent/Cutout/VertexLit"
}
