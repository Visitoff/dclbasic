﻿
		sampler2D _MainTex;
		sampler2D _Normal;
		sampler2D _GlossinessMap;
		samplerCUBE _Reflection;
		
		#ifdef GLOWMAP_ON
			sampler2D _GlowMap;
		#endif
		
		struct Input
		{
			float2 uv_MainTex;
			float4 color: COLOR;
			float3 worldPos;
			float3 worldRefl;
			INTERNAL_DATA
		};

		half _Glossiness;
		half _Metallic;
		half _Radiosity;
		half _Bump;
		fixed4 _Color;
		half _ReflectionForce;
		half _ReflectionSmooth;
		
		#ifdef GLASS
			void ReflToAlpha (Input IN, SurfaceOutputStandard o, inout fixed4 color)
			{
				color.a = (o.Emission.r + o.Emission.g + o.Emission.b) / 3;
			}
		#endif
		
		void surf (Input IN, inout SurfaceOutputStandard o)
		{
			
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			
			#ifdef VERTEXCOLOR_ON
				c.rgb *= IN.color.rgb;
			#endif
			
			fixed gloss = saturate(tex2D (_GlossinessMap, IN.uv_MainTex).r*4);
			
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness * gloss;
			
			#ifdef RADIOSITI_FROM_ALPHA_ON

				fixed ao = saturate(0.2 + IN.color.a * _Radiosity);
				o.Occlusion = ao;
				
				#ifdef DEBUG_RADIOSITI_ON
					c.rgb = fixed3(ao,ao,ao);
				#endif
			#endif
						
			o.Albedo = c.rgb;
			
			#ifdef WATER_ANIMATION_ON

				fixed3 n = UnpackNormal(tex2D (_Normal, IN.uv_MainTex + float2(_Time.x,_Time.y) * 0.1));
				fixed3 n1 = UnpackNormal(tex2D (_Normal, IN.uv_MainTex + float2(_Time.y*0.12,_Time.y*0.09)));
				
				n = (n + n1) / 2;
			#else
				fixed3 n = UnpackNormal(tex2D (_Normal, IN.uv_MainTex));
			#endif
				
			n.xy *= _Bump;
			o.Normal = n;
			
			#ifdef GLOWMAP_ON
				o.Emission = c.rgb * tex2D (_GlowMap, IN.uv_MainTex).rgb;
			#endif
			
			#ifdef REFLECTIONS_ON
				
				/*
				float4 coords;
				coords.xyz = WorldReflectionVector (IN, normalize(o.Normal));
				coords.w = _ReflectionSmooth * o.Smoothness;
				
				#ifdef REFLECTIONSMASK_ON
					o.Emission = texCUBElod (_Reflection, coords).rgb * _ReflectionForce * gloss;
				#else
					o.Emission = texCUBElod (_Reflection, coords).rgb * _ReflectionForce;
				#endif
				
				c.a += (o.Emission.r + o.Emission.g + o.Emission.b) / 3; // светлые детали на стеклах непрозрачны
				*/
				
			#endif
			
			o.Alpha = c.a;
		}

