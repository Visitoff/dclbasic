﻿Shader "Custom/CustomStandardAlphablend"
{
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Normal ("Normal", 2D) = "bump" {}
		_GlossinessMap ("GlossinessMap", 2D) = "white" {}
		_Reflection ("ReflectionMap", CUBE) = "" {}
		_ReflectionForce ("Reflection force", Range(0,1)) = 1
		_ReflectionSmooth ("Reflection smooth", Range(0,10)) = 0
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_Radiosity ("Radiosity force", Range(0,1)) = 0.8
		_Bump ("Bump", Range(0,10)) = 1.0
		[Toggle] REFLECTIONS("Use reflections", Float) = 0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		//Blend SrcAlpha OneMinusSrcAlpha
		Blend One One
		
		CGPROGRAM
		
		#pragma surface surf Standard fullforwardshadows keepalpha 

		#pragma target 3.0
		
		#pragma multi_compile REFLECTIONS_ON __
		
		#define ALPHABLEND
		
		#include "CGbase.cginc"
		
		ENDCG
	}
	FallBack "Diffuse"
}
