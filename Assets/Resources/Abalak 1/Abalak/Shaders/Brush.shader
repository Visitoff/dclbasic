﻿Shader "Hidden/Brush"
{
	Properties
	{
		_Color ("Color", Color) = (0,0,0,0)
		_MainTex ("Texture", 2D) = "white" {}
		_BrushPos ("Brush position", Vector) = (0,0,0,0)
		_BrushRadius ("Brush radius", Float) = 5
		_BrushAlpha ("Brush Alpha", Range(0,2)) = 1
		_BlendOp ("Blend Op", Float) = 0.0
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always
		Blend One One
		BlendOp [_BlendOp]
		
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			float4 _BrushPos;
			float _BrushRadius;
			float _BrushAlpha;
			fixed4 _Color;
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 c = fixed4(0,0,0,0);
				
				float f = 1.0 - saturate(length(i.uv.xy-_BrushPos.xz)/_BrushRadius);
				
				c.rgb = f * _BrushAlpha * _Color.rgb;
				
				return c;
			}
			ENDCG
		}
	}
}
