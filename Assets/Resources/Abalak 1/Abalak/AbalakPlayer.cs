﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbalakPlayer : MonoBehaviour
{

	Camera cam;
	CharacterController body;

	float rx, ry;

	void Start ()
	{
		cam = Camera.main;
		body = GetComponent<CharacterController>();
	}
	
	void Update ()
	{
		if (Input.GetMouseButton(0))
		{
			rx -= Input.GetAxis("Mouse Y") * 10;
			ry += Input.GetAxis("Mouse X") * 10;
		}

		var move = Vector3.zero;

		move.z += Input.GetKey(KeyCode.W) ? 1 : 0;
		move.z -= Input.GetKey(KeyCode.S) ? 1 : 0;
		move.x += Input.GetKey(KeyCode.D) ? 1 : 0;
		move.x -= Input.GetKey(KeyCode.A) ? 1 : 0;

		move *= 45f * 0.277778f; // 45 km/h

		move.y = -100;

		body.Move(Quaternion.AngleAxis(ry, Vector3.up) * move * Time.deltaTime);

		if (Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();

	}

	void LateUpdate()
	{
		cam.transform.position = transform.position + new Vector3(0, .75f, 0);
		cam.transform.rotation = Quaternion.Euler(rx, ry, 0);
	}

}
