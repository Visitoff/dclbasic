﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Forest : MonoBehaviour
{
	[System.Serializable]
	public class Tree
	{
		public int seed = 0;
		public Vector3 pos;
		public bool deleted = false;
	}
	
	[System.Serializable]
	public class Chunk
	{
		public int type = 0;
		public int ix, iz;
		public bool modified = true;
		public List<Tree> trees = new List<Tree>();
		public Mesh mesh;
		public GameObject gameObject;

		public void ClearGeometry()
		{
			if (gameObject != null)
				DestroyImmediate(gameObject);
			if (mesh != null)
				DestroyImmediate(mesh);
		}

	}

	public int chunkSize = 1000;
	public int addTreeType = 0;

	public List<Chunk> chunks = new List<Chunk>();

	public Material[] treeTypes;

	public bool debugTrees = true;

	public float paintRadius = 5;
	[Range(0,1)]
	public float paintDensity = 1;
	public float minDistanceBetween = 5;

	[SerializeField]
	int wasChunkSize = 1000;

	public void Clear()
	{
		foreach (var c in chunks)
		{
			c.ClearGeometry();
		}

		chunks.Clear();
	}

	public Tree AddTree(Vector3 pos)
	{
		var ix = (int)((pos.x + 100000) / chunkSize);
		var iz = (int)((pos.z + 100000) / chunkSize);

		Chunk c = null;

		for (int i = 0; i < chunks.Count; i++)
		{
			var sc = chunks[i];
			if (sc.ix == ix && sc.iz == iz && sc.type == addTreeType && sc.trees.Count < 16379)
			{
				c = sc;
				break;
			}
		}

		if (c == null)
		{
			c = new Chunk() { ix = ix, iz = iz, type = addTreeType };
			chunks.Add(c);
		}

		var t = new Tree();
		t.pos = pos;
		t.seed = Random.Range(0, 100000);
		c.trees.Add(t);

		c.modified = true;

		return t;
	}

	public void FillRandom()
	{
		float area=100;
		for (int i = 0; i < 1000; i++)
		{
			var pos = new Vector3(Random.Range(-area, area), 10000, Random.Range(-area, area));
			var ray = new Ray(pos, Vector3.down);
			RaycastHit info;
			if (Physics.Raycast(ray, out info, 20000f))
			{
				AddTree(info.point);
			}
		}
		RebuildMeshes();
	}

	public void RemoveTreesByMinDistanceBetween()
	{

		var distSqr = minDistanceBetween * minDistanceBetween;

		foreach (var c in chunks)
		{
			foreach (var t in c.trees)
			{

				if (!t.deleted)
				{
					foreach (var cc in chunks)
					{
						foreach (var tt in cc.trees)
						{
							if (!tt.deleted && tt != t)
							{
								if ((t.pos - tt.pos).sqrMagnitude < distSqr)
								{
									t.deleted = true;
									break;
								}
							}
						}

						if (t.deleted)
							break;
					}
				}
			}

			c.trees.RemoveAll(t => t.deleted);
			c.modified = true;
		}

		RebuildMeshes();

	}

	public void RemoveInArea(Vector3 pos)
	{
		var radiusSqr = paintRadius * paintRadius;

		foreach (var c in chunks)
		{
			
			foreach (var t in c.trees)
			{
				if ((t.pos - pos).sqrMagnitude < radiusSqr)
				{
					t.deleted = true;
					c.modified = true;
				}
			}

			if (c.modified)
				c.trees.RemoveAll(dt => dt.deleted);
		}
	}

	void OnDrawGizmos()
	{
		if (debugTrees)
		{
			foreach (var c in chunks)
			{
				foreach (var t in c.trees)
				{
					Gizmos.DrawLine(t.pos, t.pos + new Vector3(0, 10, 0));
				}
			}
		}
	}

	public void ForceRebuildMeshes()
	{
		foreach (var c in chunks)
			c.modified = true;
		RebuildMeshes();
	}

	void CheckChunkSizeChanged()
	{
		if (wasChunkSize != chunkSize)
		{
			wasChunkSize = chunkSize;

			var alltrees = new List<Tree>();

			foreach (var c in chunks)
				foreach (var t in c.trees)
					alltrees.Add(t);

			Clear();

			foreach (var t in alltrees)
			{
				var nt = AddTree(t.pos);
				nt.seed = t.seed;
			}

		}
	}

	public void RebuildMeshes()
	{

		CheckChunkSizeChanged();

		foreach (var c in chunks)
		{
			if (!c.modified)
				continue;

			c.modified = false;

			c.ClearGeometry();

			c.mesh = new Mesh();

			var mat = GetTreeTypeById(c.type);

			var centers = new Vector3[c.trees.Count * 4];
			var corners = new Vector3[centers.Length];
			var uvs = new Vector2[centers.Length];
			var attenuations = new Vector2[centers.Length];
			var inds = new int[c.trees.Count * 6];

			int vertIndex = 0;
			int quadIndex = 0;

			float baseSize = GetMaterialFloatProperty(mat, "_Size", 10);
			float sizeRandomize = GetMaterialFloatProperty(mat, "_SizeRandomize", 0);
			float yShift = GetMaterialFloatProperty(mat, "_YShift", 0);
			float brightnessRandomize = GetMaterialFloatProperty(mat, "_BrightnessRandomize", 0);

			foreach (var t in c.trees)
			{

				Random.InitState(t.seed);

				var va = vertIndex;
				var vb = vertIndex + 1;
				var vc = vertIndex + 2;
				var vd = vertIndex + 3;

				var size = baseSize * (1f + Random.Range(-sizeRandomize, sizeRandomize));

				var yshift = Vector3.up * (size * 0.5f);
				yshift.y += size * yShift;

				centers[va] = t.pos + yshift;
				centers[vb] = t.pos + yshift;
				centers[vc] = t.pos + yshift;
				centers[vd] = t.pos + yshift;

				corners[va] = srcCorners[0] * size;
				corners[vb] = srcCorners[1] * size;
				corners[vc] = srcCorners[2] * size;
				corners[vd] = srcCorners[3] * size;

				uvs[va] = srcUVs[0];
				uvs[vb] = srcUVs[1];
				uvs[vc] = srcUVs[2];
				uvs[vd] = srcUVs[3];

				var att = Vector2.zero;
				att.x = (1f - Random.value * brightnessRandomize);
				att.y = Random.value;

				attenuations[va] = att;
				attenuations[vb] = att;
				attenuations[vc] = att;
				attenuations[vd] = att;

				inds[quadIndex] = srcInds[0] + vertIndex;
				inds[quadIndex + 1] = srcInds[1] + vertIndex;
				inds[quadIndex + 2] = srcInds[2] + vertIndex;
				inds[quadIndex + 3] = srcInds[3] + vertIndex;
				inds[quadIndex + 4] = srcInds[4] + vertIndex;
				inds[quadIndex + 5] = srcInds[5] + vertIndex;

				vertIndex += 4;
				quadIndex += 6;

			}

			c.mesh = new Mesh();

			c.mesh.vertices = centers;
			c.mesh.normals = corners;
			c.mesh.uv = uvs;
			c.mesh.uv2 = attenuations;
			c.mesh.triangles = inds;

			c.gameObject = new GameObject("Chunk");
			c.gameObject.transform.parent = transform;

			c.gameObject.AddComponent<MeshFilter>().sharedMesh = c.mesh;
			c.gameObject.AddComponent<MeshRenderer>().sharedMaterial = mat;
		}
	}

	Material GetTreeTypeById(int id)
	{
		if(treeTypes==null)
			return null;

		if (treeTypes.Length == 0)
			return null;

		if (id < treeTypes.Length)
			return treeTypes[id];

		return null;
	}

	float GetMaterialFloatProperty(Material mat, string propName, float defaultValue)
	{
		if (mat == null)
			return defaultValue;

		return mat.GetFloat(propName);
	}

	static Vector3[] srcCorners = new Vector3[]
	{
		new Vector3(-0.5f, -0.5f, 0.0f),
		new Vector3(0.5f, 0.5f, 0.0f),
		new Vector3(0.5f, -0.5f, 0.0f),
		new Vector3(-0.5f, 0.5f, 0.0f),
	};

	static Vector2[] srcUVs = new Vector2[]
	{
		new Vector2(0.0f, 0.0f),
		new Vector2(1.0f, 1.0f),
		new Vector2(1.0f, 0.0f),
		new Vector2(0.0f, 1.0f)
	};

	static int[] srcInds = new int[]
	{
		0,1,2,1,0,3
	};

}
