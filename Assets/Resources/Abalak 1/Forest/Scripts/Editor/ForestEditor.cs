﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Forest))]
public class ForestEditor : Editor
{
	Forest f;
	Event e;

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		f = target as Forest;

		GUILayout.BeginVertical();

		But("Clear", () => { f.Clear(); });
		But("Remove by min distace", () => f.RemoveTreesByMinDistanceBetween());
		But("Force Rebuild", () => f.ForceRebuildMeshes());

		GUILayout.EndVertical();

	}
	
	void But(string buttonCaption, System.Action onClick)
	{
		if (GUILayout.Button(buttonCaption))
			onClick();
	}

	protected virtual void OnSceneGUI()
	{
		
		f = target as Forest;
		e = Event.current;
		
		if (e.type == EventType.Layout)
			HandleUtility.AddDefaultControl(GUIUtility.GetControlID(GetHashCode(), FocusType.Passive));

		switch (e.type)
		{
			case EventType.Layout:
				RaycastScene(HandleUtility.GUIPointToWorldRay(e.mousePosition), r => mousePosition3D = r);
				break;
			case EventType.Repaint:
				Handles.CircleHandleCap(0, mousePosition3D, Quaternion.AngleAxis(90, Vector3.right), f.paintRadius, EventType.Repaint);
				Handles.DrawLine(mousePosition3D, mousePosition3D + new Vector3(0, 20, 0));
				break;
			case EventType.ScrollWheel:

				if (e.modifiers == EventModifiers.Shift)
				{
					f.paintRadius -= e.delta.y * f.paintRadius * .1f;
					e.Use();
				}

				break;
			case EventType.MouseDown:
			case EventType.MouseDrag:

				//if (SceneView.currentDrawingSceneView.position.Contains(e.mousePosition))
				{

					if (e.button == 0 && !e.alt)
					{
						if (e.control && e.shift)
						{
						}
						else if (e.control)
						{
							f.RemoveInArea(mousePosition3D);
							f.RebuildMeshes();
						}
						else if (e.shift)
						{
							if (e.type == EventType.MouseDown)
							{
								f.AddTree(mousePosition3D);
								f.RebuildMeshes();
							}
						}
						else
						{
							var count = f.paintRadius * f.paintDensity;
							for (int i = 0; i < count; i++)
							{
								var pos = mousePosition3D;
								var shift = Random.insideUnitCircle*f.paintRadius;
								pos.x+=shift.x;
								pos.z+=shift.y;
								var ray = new Ray(pos, Vector3.down);

								RaycastScene(ray, r => f.AddTree(r));
							}
							
							f.RebuildMeshes();
						}

						e.Use();
					}
				}
				break;
			case EventType.KeyDown:

				switch (e.keyCode)
				{
						/*
					case KeyCode.Alpha1:
						mp.channel = 0;
						e.Use();
						break;
					case KeyCode.Alpha2:
						mp.channel = 1;
						e.Use();
						break;
					case KeyCode.Alpha3:
						mp.channel = 2;
						e.Use();
						break;
					case KeyCode.Alpha4:
						mp.channel = 3;
						e.Use();
						break;
						 */
				}

				break;
		}

		if (e.type == EventType.Layout)
			HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
	}

	Vector3 mousePosition3D;

	bool RaycastScene(Ray ray, System.Action<Vector3> result)
	{
		RaycastHit info;
		if (Physics.Raycast(ray, out info, Mathf.Infinity))
		{
			result(info.point);
			return true;
		}
		return false;
	}

}
