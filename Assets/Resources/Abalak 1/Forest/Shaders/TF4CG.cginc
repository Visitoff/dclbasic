﻿
			struct appdata
			{
				float4 center : POSITION;
				float3 corner : NORMAL;
				float2 uv : TEXCOORD0;
				float2 attenuations : TEXCOORD1;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float3 uv : TEXCOORD0;
				
				#ifndef TF4_SHADOW
					
					UNITY_FOG_COORDS(1)
					
					#ifndef ATT_UPNORMAL
						float3 n : TEXCOORD2;
					#endif
					
					#if defined(ATT_SHARP) || defined(ATT_SMOOTH)
						float3 t : TEXCOORD3;
						float3 b : TEXCOORD4;
					#endif
					
					#if defined(POINT) || defined(SPOT)
						float4 posLight : TEXCOORD5;
						float3 lightDir : TEXCOORD6;
					#endif
				
				#endif
			};

			sampler2D _MainTex;
			fixed4 _Color;
			float4 _LookAtCenter;
			
			#ifndef TF4_SHADOW
				
				sampler2D _Bump;
				float4 _LightColor0;
				float4x4 unity_WorldToLight;
				sampler2D _LightTexture0; 
				sampler2D _LightTextureB0;
				
			#endif
			
			v2f vert (appdata v)
			{
				v2f o;
				
				UNITY_INITIALIZE_OUTPUT(v2f, o);
				
				//#define LOOKAT_CENTER _LookAtCenter.xyz
				#define LOOKAT_CENTER _WorldSpaceCameraPos.xyz
				
				#ifdef TF4_SHADOW
					float3 camVect;
					if(unity_MatrixVP[3][3]==1)
						camVect = _WorldSpaceLightPos0.w < 0.5 ? _WorldSpaceLightPos0.xyz : mul(unity_ObjectToWorld, v.center).xyz - _WorldSpaceLightPos0.xyz;
					else
						camVect = mul(unity_ObjectToWorld, v.center).xyz - LOOKAT_CENTER;
				#else
					float3 camVect = mul(unity_ObjectToWorld, v.center).xyz - LOOKAT_CENTER;
				#endif

				#if defined(BILLBOARDMODE_CYLINDRICAL) || defined(BILLBOARDMODE_SPEEDTREE)
					camVect.y = 0;
				#endif
				
				// calculations of lookat and frame from texture are same for shadow and diffuse shader
				// in case with shadow pass billboards facing light position
				
				// LookAt matrix
				float3 zaxis = normalize(camVect);
				float3 xaxis = normalize(cross(float3(0, 1, 0), zaxis));
				float3 yaxis = cross(zaxis, xaxis);

				float4x4 lookatMatrix = {
					xaxis.x,            yaxis.x,            zaxis.x,       0,
					xaxis.y,            yaxis.y,            zaxis.y,       0,
					xaxis.z,            yaxis.z,            zaxis.z,       0,
					0, 0, 0,  1
				};
				
				v.center += mul(lookatMatrix, float4(v.corner.xy, 0, 0));
				
				o.pos = UnityObjectToClipPos(v.center);
				
				o.uv.xy = v.uv;
				
				#ifdef BILLBOARDMODE_SPHERICAL
					
					// only in spherical mode frames lookups possible
					
					o.uv.xy /= 4.0;
					
					fixed d = saturate(-dot(zaxis, fixed3(0, 1, 0)));
				
					int row = (int)(d * 4);
					int col = (int)(d * 16);

					o.uv.y += 0.25 * row;
					o.uv.x += 0.25 * (col - row * 4);
					
				#endif
				
				#ifdef BILLBOARDMODE_SPEEDTREE
					
					// special case, SpeedTree atlases is 3x3
					
					o.uv.x /= 4.0;
					
					o.uv.x += 0.25 * ((int)(v.attenuations.x * 4));
					/*
					fixed d = saturate(-dot(zaxis, fixed3(0, 1, 0)));
				
					int row = (int)(d * 4);
					int col = (int)(d * 16);

					o.uv.y += 0.25 * row;
					o.uv.x += 0.25 * (col - row * 4);
					*/
				#endif
				
				// for shadow pass no need light calculations, skipping it
				#ifndef TF4_SHADOW
					
					#ifndef ATT_UPNORMAL // for SHARP, SMOOTH and FLAT modes need tree normal
						o.n = -zaxis;
					#endif
					
					#if defined(ATT_SHARP) || defined(ATT_SMOOTH) // FLAT and UPNORMAL modes not using normalmap texture, tanget and binormal not needed
						o.b = yaxis;
						o.t = xaxis;
					#endif
					
					o.uv.z = v.attenuations.x;
					
					#if defined(POINT) || defined(SPOT)
						o.lightDir = normalize(_WorldSpaceLightPos0.xyz - v.center.xyz);
						o.posLight = mul(unity_WorldToLight, v.center);
					#endif
					
					UNITY_TRANSFER_FOG(o,o.pos);
					
				#endif
				
				return o;
			}
			
			#ifdef ATT_SHARP 
				#define TF4_ATTENUATION(n,lightDir) max(0,(dot(n,lightDir))) // usual Unity light attenuation
			#else 
				#define TF4_ATTENUATION(n,lightDir) saturate((dot(n,lightDir)+1.0)*0.5) // smooth attenuation, simulates some kind translucency, where tree even from back side affected by lights
			#endif
			
			fixed4 frag (v2f i) : SV_Target
			{
				
				fixed4 col = tex2D(_MainTex, i.uv);
				//col.rgb=fixed3(1,1,1);
				clip(col.a * _Color.a - .1);
				
				// all lights and fog not affect shadow pass
				#ifndef TF4_SHADOW
					
					col.rgb *= _Color.rgb * i.uv.z; // brightness variation
					
					// normal of tree
					#if defined(ATT_SHARP) || defined(ATT_SMOOTH)
						
						fixed3 bump = tex2D(_Bump, i.uv).rgb * 2.0 - 1.0;
						bump.z = saturate(abs(bump.z)+.25);
						bump = i.n * bump.z + i.b * bump.y + i.t * bump.x;
					
					#else 
						
						// FLAT and UPNORMAL attenuation, skipped normalmap lookups and calculations, only tree normal direction used
						
						#ifdef ATT_UPNORMAL
							#define bump fixed3(0,1,0) // in case of UPNORMAL Vector3.up used for all trees
						#else
							#define bump i.n // in case of FLAT tree normal is vector from tree to camera
						#endif
					
					#endif
					
					// lights
					#ifndef TF4_FORWARD_ADD // base pass only (ambient color + directional light + fog) in any case
						
						col.rgb *= UNITY_LIGHTMODEL_AMBIENT.rgb + (_LightColor0.rgb * TF4_ATTENUATION(bump, normalize(_WorldSpaceLightPos0.xyz)));
						
						UNITY_APPLY_FOG(i.fogCoord, col);						
					
					#else // additive passes for all lights
						
						#ifdef DIRECTIONAL
							col.rgb *= _LightColor0.rgb * TF4_ATTENUATION(bump, normalize(_WorldSpaceLightPos0.xyz));							
						#endif
					
						#ifdef SPOT
							#define attenuation (((i.posLight.z > 0) * tex2D(_LightTextureB0, dot(i.posLight, i.posLight).xx).UNITY_ATTEN_CHANNEL) * tex2D(_LightTexture0, i.posLight.xy / i.posLight.w + 0.5).a)
							col.rgb *= _LightColor0.rgb * (attenuation * TF4_ATTENUATION(bump, normalize(i.lightDir)));
						#endif
						
						#ifdef POINT
							#define attenuation (tex2D(_LightTexture0, dot(i.posLight.xyz, i.posLight.xyz).rr).UNITY_ATTEN_CHANNEL)
							col.rgb *= _LightColor0.rgb * (attenuation * TF4_ATTENUATION(bump, normalize(i.lightDir)));
						#endif
						
					#endif
					
				#endif
				
				return col;
			}
			


