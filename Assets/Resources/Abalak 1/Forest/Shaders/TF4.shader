﻿Shader "TF4/Billboard"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Texture", 2D) = "white" {}
		_Bump ("Bump", 2D) = "white" {}
		_Size ("Size", Float) = 10
		_SizeRandomize ("Size Randomize", Float) = .5
		_YShift ("Y Shift", Float) = 0
		_BrightnessRandomize ("Brightness Randomize", Float) = .5
		
		_LookAtCenter ("LookAt center", Vector) = (0,0,0,0)
		
		[KeywordEnum(SHARP, SMOOTH, FLAT, UPNORMAL)] ATT ("Attenuation type", Float) = 0
		[KeywordEnum(SPHERICAL, CYLINDRICAL, SPEEDTREE)] BILLBOARDMODE ("Billboard mode", Float) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "Queue"="Alphatest"}
		LOD 100

		Pass
		{
			Name "FORWARD"
			Tags { "LightMode" = "ForwardBase"}
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog
			#pragma multi_compile ATT_SHARP ATT_SMOOTH ATT_FLAT ATT_UPNORMAL
			#pragma multi_compile BILLBOARDMODE_SPHERICAL BILLBOARDMODE_CYLINDRICAL BILLBOARDMODE_SPEEDTREE
			
			#include "UnityCG.cginc"
			#include "TF4CG.cginc"
			
			ENDCG
		}
		
		Pass
		{
			Name "FORWARD"
			Tags { "LightMode" = "ForwardAdd"}
			
			ZWrite Off Blend One One
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile POINT SPOT DIRECTIONAL
			#pragma multi_compile ATT_SHARP ATT_SMOOTH ATT_FLAT ATT_UPNORMAL
			#pragma multi_compile BILLBOARDMODE_SPHERICAL BILLBOARDMODE_CYLINDRICAL BILLBOARDMODE_SPEEDTREE
			
			#define TF4_FORWARD_ADD
			
			#include "UnityCG.cginc"
			#include "TF4CG.cginc"
			
			ENDCG
		}
		
		Pass
		{
			Name "ShadowCaster"
			Tags { "LightMode" = "ShadowCaster" }
			Fog {Mode Off}
			ZWrite On ZTest LEqual Cull Back
			Offset 1, 1
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile BILLBOARDMODE_SPHERICAL BILLBOARDMODE_CYLINDRICAL BILLBOARDMODE_SPEEDTREE
			
			#define TF4_SHADOW
			
			#include "UnityCG.cginc"
			#include "TF4CG.cginc"
			ENDCG
		}
		
	}
	
	Fallback "Diffuse"
}
